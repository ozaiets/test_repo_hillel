package Interfaces;

public interface Motorable {

    void startMotor();
    void stopMotor();
}
