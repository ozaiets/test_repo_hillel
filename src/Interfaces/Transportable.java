package Interfaces;

public interface Transportable {

    int startingSpeed = 0;

    void ride(int speed);
    void stop();
}
