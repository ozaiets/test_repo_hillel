class MotorBoat extends MotorizedVehicle {

    int weight;

    MotorBoat() {
        super();
        String type = "MotorBoat";
        setType(type);
    }

    MotorBoat(int weight) {
        this();
        this.weight = weight;
    }

    @Override
    public void ride(int speed) {
        if (weight > 1000) speed *= 0.7;
        super.ride(speed);
    }

    public void ride(int speed, boolean isOnStream) {
        if (isOnStream) speed *= 1.2;
        super.ride(speed);
    }
}
