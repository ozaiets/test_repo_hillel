import Interfaces.Motorable;
import Interfaces.Transportable;

abstract class MotorizedVehicle extends Vehicle implements Transportable, Motorable {

    private Motor motor;
    protected double fuelSupply;

    MotorizedVehicle() {
        super();
        this.speed = startingSpeed;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public Motor getMotor() {
        return this.motor;
    }

    @Override
    public void ride(int speed) {
        this.startMotor();
        this.speed = speed;
        if (this.fuelSupply < (speed/10)) addFuel(speed/10);
        motor.mileage += speed;
    }

    @Override
    public void stop() {
        this.speed = startingSpeed;
        this.stopMotor();
    }

    public void addFuel(double fuel) {
        this.fuelSupply += fuel;
        System.out.println("Fuel was added. Current fuel supply " + this.fuelSupply);
    }

    public void startMotor() {
        if (motor != null && fuelSupply > 0 && motor.mileage < 500000) {
            motor.isStarted = true;
            System.out.println("Motor turned on");
        }
        else {
            motor.isStarted = false;
            System.out.println("Can't start the motor. Caused by fuel lack or motor failure");
        }
    }

    public void stopMotor() {
        motor.isStarted = false;
        System.out.println("Motor turned off");
    }

    final class Motor {

        String fuelType;
        int mileage;
        boolean isStarted;

        Motor() {
            this.isStarted = false;
            this.mileage = 0;
            this.fuelType = "petrol";

        }

        Motor(String fuelType, int mileage) {
            this.fuelType = fuelType;
            this.mileage = mileage;
            this.isStarted = false;
        }
    }
}
