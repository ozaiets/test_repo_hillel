import Interfaces.Transportable;

public final class Snowboard extends Vehicle implements Transportable {

    double speed;

    Snowboard() {
        this.setType("Snowboard");
    }

    @Override
    public void ride(int speed) {
        this.speed = speed;
    }

    @Override
    public void stop() {
        this.speed = startingSpeed;
    }

    public void ride(int speed, int slopeAngle) {
        if(slopeAngle > 5) this.speed = speed * 1.2;
    }
}
