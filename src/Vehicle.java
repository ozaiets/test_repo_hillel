public abstract class Vehicle {

    private String type;
    protected String brand;
    protected String model;
    protected int speed;
    protected int maxSpeed;

    protected String getType() {
        return this.type;
    }

    protected void setType(String type) {
        this.type = type;
    }

    public Vehicle() {
        this.type = "undefined";
    }

    public Vehicle(String type, String brand, String model, int maxSpeed) {
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

}
